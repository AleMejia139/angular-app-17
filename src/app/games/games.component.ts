import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-games',
  standalone: true,
  imports: [CommonModule, RouterOutlet],
  template: `
    <p>
      <b> Forma antigua a usar de Angular </b>
      Games is a collection of games that are not part of the main app. They can
      be played in this window or opened in their own windowsGames Games!
      <a routerLink="./list">List</a> |
      <a routerLink="./form">Form</a>
    </p>
    <h3>Los juegos favoritos de: {{ username }}</h3>
    <ul>
      <b> Forma nueva de Angular </b>
      @for (game of games; track game.id) {
      <li (click)="fav(game.name)">{{ game.name }}</li>
      }
    </ul>
  `,
  styles: ``,
})
export class GamesComponent {
  @Input() username = '';
  @Output() favGame = new EventEmitter<string>();

  fav(gameName: string) {
    // Forma alterna de hacer un alert alert(`A ${this.username} le gusta jugar ${gameName}`);
    this.favGame.emit(gameName);
  }

  games = [
    {
      id: 1,
      name: 'Super Smash Bros. Ultimate',
    },
    {
      id: 2,
      name: 'The Legend of Zelda: Tears of the Kingdom',
    },
    {
      id: 3,
      name: 'Horizon Zero Dawn',
    },
    {
      id: 4,
      name: 'Halo Infinite ',
    },
  ];
}
